use crate::geometry::Segment;
use crate::movement::{self, Collidable, HitBox};
use crate::sprite::{AnimData, AnimFrame, InstanceData, NoAnim, Sprite};
use crate::util::*;
use macroquad::prelude::*;

const PLAYER_SIZE: f64 = 1.5;
const ENEMY_SIZE: f64 = 2.0;
const AMBIENT_SIZE: f64 = 0.5;
const PLAYER_LOC: &'static str = "player.png";
const ENEMY_LOC: &'static str = "enemy.png";
const FISH_LOC: &'static str = "fish.png";
const PLAYER_ACCEL: f64 = 2.5;
const PLAYER_DRAG: f64 = 0.5;
const MIN_ENEMY_SPEED: f64 = 0.5;
const MAX_ENEMY_SPEED: f64 = 5.0;
const MAX_ENEMY_ANG_VEL: f64 = 6.28;
const MIN_ENEMY_ANG_VEL: f64 = 0.628;
const MAX_ENEMY_ROT_RADIUS: f64 = 5.0;
const MIN_ENEMY_ROT_RADIUS: f64 = 1.0;
const FISH_SPEED: f64 = 1.0;

fn gen_pos(size: f64) -> DVec2 {
	movement::allowed_region(DVec2::splat(size)).gen_in()
}

pub struct Character {
	pos: DVec2,
	size: DVec2,
	pub sprite: Sprite
}

impl Character {
	pub async fn new(loc: &str, ad: AnimData, pos: DVec2, size: DVec2) -> Self {
		Self {
			pos,
			size,
			sprite: Sprite::new(loc, ad).await
		}
	}

	pub fn set_pos(&mut self, pos: DVec2) {
		self.pos = pos
	}

	pub fn set_frame<T: AnimFrame>(&mut self, t: T) {
		self.sprite.set_frame(t)
	}
}

impl Collidable for Character {
	fn hitbox(&self) -> HitBox {
		HitBox::new(self.pos, self.size)
	}
}

impl Drawable<()> for Character {
	fn draw(&self, _: ()) {
		let data = InstanceData::new_aligned(self.pos, self.size);
		self.sprite.draw(data)
	}
}

#[derive(Clone, Copy)]
#[repr(usize)]
pub enum PlayerAnim {
	Aligned = 0,
	Diagonal = 1
}

impl AnimFrame for PlayerAnim {
	fn frame(&self) -> usize {
		*self as usize
	}

	fn data() -> AnimData {
		AnimData::new(1, 2, 2)
	}
}

pub struct Player {
	inner: Character,
	pub vel: DVec2,
	side_bounce_due: bool
}

impl Player {
	pub async fn new() -> Self {
		let ad = PlayerAnim::data();
		let size = DVec2::splat(PLAYER_SIZE);
		let mut out = Self {
			inner: Character::new(&PLAYER_LOC, ad, DVec2::ZERO, size).await,
			vel: DVec2::ZERO,
			side_bounce_due: false
		};
		out.respawn();
		out
	}

	pub fn respawn(&mut self) {
		self.inner.set_pos(movement::left(DVec2::splat(PLAYER_SIZE), true));
		self.vel = DVec2::ZERO;
		self.inner.sprite.set_frame(PlayerAnim::Aligned);
		self.side_bounce_due = false
	}

	pub fn request_side_bounce(&mut self) {
		self.side_bounce_due = true
	}

	pub fn pos(&self) -> DVec2 {
		self.inner.pos
	}
}

impl Integrable for Player {
	fn step(&mut self, dt: f64) {
		let mut control = DVec2::ZERO;
		if is_key_down(KeyCode::Left) || is_key_down(KeyCode::A) {
			control -= DVec2::X
		};
		if is_key_down(KeyCode::Right) || is_key_down(KeyCode::D) {
			control += DVec2::X
		};
		if is_key_down(KeyCode::Up) || is_key_down(KeyCode::W) {
			control += DVec2::Y
		};
		if is_key_down(KeyCode::Down) || is_key_down(KeyCode::S) {
			control -= DVec2::Y
		};
		self.vel += control * dt * PLAYER_ACCEL;
		let b0 = movement::collides_left(&*self) && self.vel.x < 0.0;
		let b1 = movement::collides_right(&*self) && self.vel.x > 0.0;
		if b0 || b1 || self.side_bounce_due {
			self.vel.x *= -1.0;
			self.side_bounce_due = false
			// TODO play sound
		};
		let b2 = movement::collides_top(&*self) && self.vel.y > 0.0;
		let b3 = movement::collides_bottom(&*self) && self.vel.y < 0.0;
		if b2 || b3 {
			self.vel.y *= -1.0
			// TODO play sound
		};
		self.vel *= 1.0 - PLAYER_DRAG * dt;
		self.inner.set_pos(self.inner.pos + self.vel * dt);
		if self.vel.x == 0.0 {
			self.inner.set_frame(PlayerAnim::Aligned)
		}
		else {
			let t = (self.vel.y / self.vel.x).abs();
			let p8 = std::f64::consts::PI * 0.125;
			let (tp8, t3p8) = (p8.tan(), (3.0 * p8).tan());
			if t < tp8 || t >= t3p8 {
				self.inner.set_frame(PlayerAnim::Aligned)
			}
			else {
				self.inner.set_frame(PlayerAnim::Diagonal)
			}
		}
	}
}

impl Collidable for Player {
	fn hitbox(&self) -> HitBox {
		self.inner.hitbox()
	}
}

impl Drawable<()> for Player {
	fn draw(&self, _: ()) {
		self.inner.draw(())
	}
}

#[derive(Clone, Copy)]
pub enum EnemyType {
	Stationary(DVec2),
	// Path, angular frequency
	Linear(Segment, f64),
	// Centre, radius, angular velocity
	Rotating(DVec2, f64, f64)
}

impl EnemyType {
	fn gen_pos() -> DVec2 {
		gen_pos(PLAYER_SIZE + ENEMY_SIZE)
	}

	pub fn gen_stationary() -> Self {
		Self::Stationary(Self::gen_pos())
	}

	pub fn gen_linear() -> Self {
		let s = Segment(Self::gen_pos(), Self::gen_pos());
		let v = rand::gen_range(MIN_ENEMY_SPEED, MAX_ENEMY_SPEED);
		Self::Linear(s, 2.0 * v / s.length())
	}

	pub fn gen_rotating() -> Self {
		let r = rand::gen_range(MIN_ENEMY_ROT_RADIUS, MAX_ENEMY_ROT_RADIUS);
		let size = DVec2::splat(ENEMY_SIZE + PLAYER_SIZE);
		let pos = movement::allowed_region(size).grow(-r).gen_in();
		let ang_vel = rand::gen_range(MIN_ENEMY_ANG_VEL, MAX_ENEMY_ANG_VEL);
		Self::Rotating(pos, r, ang_vel)
	}
}

pub struct Enemy {
	inner: Character,
	etype: EnemyType,
	time: f64
}

impl Enemy {
	pub async fn new(etype: EnemyType) -> Self {
		let ad = NoAnim::data();
		let size = DVec2::splat(ENEMY_SIZE);
		let pos = match etype {
			EnemyType::Stationary(p) => p,
			EnemyType::Linear(s, _) => s.0,
			EnemyType::Rotating(p, _, _) => p
		};
		Self {
			inner: Character::new(&ENEMY_LOC, ad, pos, size).await,
			etype,
			time: 0.0
		}
	}

	pub fn pos(&self) -> DVec2 {
		self.inner.pos
	}
}

impl Integrable for Enemy {
	fn step(&mut self, dt: f64) {
		self.time += dt;
		match self.etype {
			EnemyType::Stationary(_) => (),
			EnemyType::Linear(s, om) => {
				self.inner.set_pos(s.lerp_in((om * self.time).sin() * 0.5 + 0.5))
			},
			EnemyType::Rotating(p, r, om) => {
				let x = p.x + r * (om * self.time).cos();
				let y = p.y + r * (om * self.time).sin();
				self.inner.set_pos(DVec2::new(x, y))
			}
		}
	}
}

impl Collidable for Enemy {
	fn hitbox(&self) -> HitBox {
		self.inner.hitbox()
	}
}

impl Drawable<()> for Enemy {
	fn draw(&self, _: ()) {
		self.inner.draw(())
	}
}

#[derive(Clone, Copy)]
pub enum AmbientType {
	// Prev, heading, time
	Fish(DVec2, DVec2, f64)
}

impl AmbientType {
	pub fn gen_fish() -> Self {
		let (old, new) = (gen_pos(AMBIENT_SIZE), gen_pos(AMBIENT_SIZE));
		Self::Fish(old, new, 0.0)
	}

	pub fn res_loc(&self) -> &str {
		match *self {
			Self::Fish(_, _, _) => &FISH_LOC
		}
	}

	pub fn pos(&self) -> DVec2 {
		match *self {
			Self::Fish(old, new, time) => {
				old.lerp(new, (FISH_SPEED * time) / (new - old).length())
			}
		}
	}

	pub fn instance_data(&self) -> InstanceData {
		match *self {
			Self::Fish(old, new, _) => {
				let disp = new - old;
				InstanceData {
					pos: self.pos(),
					size: DVec2::splat(AMBIENT_SIZE),
					rotation: disp.y.atan2(disp.x)
				}
			}
		}
	}
}

impl Integrable for AmbientType {
	fn step(&mut self, dt: f64) {
		match *self {
			Self::Fish(o, n, t) => {
				let mut time = t + dt;
				let mut old = o;
				let mut new = n;
				if (n - o).length_squared() < (FISH_SPEED * time).powi(2) {
					old = n;
					new = gen_pos(AMBIENT_SIZE);
					time = 0.0
				};
				*self = Self::Fish(old, new, time)
			}
		}
	}
}

pub struct Ambient {
	inner: Character,
	atype: AmbientType
}

impl Ambient {
	pub async fn new(atype: AmbientType) -> Self {
		let ad = NoAnim::data();
		let size = DVec2::splat(AMBIENT_SIZE);
		let pos = gen_pos(AMBIENT_SIZE);
		Self {
			inner: Character::new(atype.res_loc(), ad, pos, size).await,
			atype
		}
	}
}

impl Integrable for Ambient {
	fn step(&mut self, dt: f64) {
		self.atype.step(dt);
		self.inner.set_pos(self.atype.pos())
	}
}

impl Drawable<()> for Ambient {
	fn draw(&self, _: ()) {
		self.inner.sprite.draw(self.atype.instance_data())
	}
}
