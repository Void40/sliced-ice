use crate::util::*;
use macroquad::prelude::*;
use miniquad::graphics::{BlendFactor, BlendState, BlendValue, Equation};

const DEFAULT_FS_LOC: &'static str = "default.glslf";
const DEFAULT_VS_LOC: &'static str = "default.glslv";

pub fn default_pipeline_params() -> PipelineParams {
	let v0 = BlendFactor::Value(BlendValue::SourceAlpha);
	let v1 = BlendFactor::OneMinusValue(BlendValue::SourceAlpha);
	let f = |a0, a1| Some(BlendState::new(Equation::Add, a0, a1));
	PipelineParams {
		color_blend: f(v0, v1),
		alpha_blend: f(BlendFactor::Zero, BlendFactor::One),
		.. Default::default()
	}
}

pub struct Shaders {
	pub vert: String,
	pub frag: String
}

impl Shaders {
	pub async fn new(v: Option<&str>, f: Option<&str>) -> Self {
		let vert = load_utf8(v.unwrap_or(&DEFAULT_VS_LOC)).await.unwrap();
		let frag = load_utf8(f.unwrap_or(&DEFAULT_FS_LOC)).await.unwrap();
		Self {
			vert,
			frag
		}
	}

	pub async fn none() -> Self {
		Self::new(None, None).await
	}

	pub fn load_material(&self, us: Vec<(String, UniformType)>) -> Material {
		let mp = MaterialParams {
			pipeline_params: default_pipeline_params(),
			uniforms: us,
			.. Default::default()
		};
		load_material(&self.vert, &self.frag, mp).unwrap()
	}
}

pub struct ShaderLayer {
	material: Material
}

impl ShaderLayer {
	pub async fn new(fs: &str, us: Vec<(String, UniformType)>) -> Self {
		let shaders = Shaders::new(None, Some(fs)).await;
		Self {
			material: shaders.load_material(us)
		}
	}

	pub fn set_uniform<T>(&self, name: &str, uniform: T) {
		self.material.set_uniform(name, uniform)
	}
}

impl Drawable<()> for ShaderLayer {
	fn draw(&self, _: ()) {
		let screen_size = (screen_width() as u32, screen_height() as u32);
		let render_target = render_target(screen_size.0, screen_size.1);
		gl_use_material(self.material);
		draw_texture(render_target.texture, 0.0, 0.0, WHITE);
		gl_use_default_material()
	}
}
