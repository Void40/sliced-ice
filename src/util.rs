use std::path;
use std::string::FromUtf8Error;
use macroquad::prelude::*;

const ASSETS_DIR: &'static str = "assets";
pub const SIZE_X: f64 = 20.0;
pub const SIZE_Y: f64 = 20.0;

pub fn used_width() -> f32 {
	screen_height() * (SIZE_X / SIZE_Y) as f32
}

pub fn left_offset() -> f32 {
	(screen_width() - used_width()) * 0.5
}

pub fn whole_area() -> (DVec2, DVec2) {
	(DVec2::ZERO, DVec2::new(SIZE_X, SIZE_Y))
}

pub fn to_screen_size(size: DVec2) -> Vec2 {
	let (u, v) = (size.x / SIZE_X, size.y / SIZE_Y);
	let x = u as f32 * used_width();
	let y = v as f32 * screen_height();
	Vec2::new(x, y)
}

pub fn to_screen_coords(pos: DVec2) -> Vec2 {
	let (u, v) = (pos.x / SIZE_X, pos.y / SIZE_Y);
	let x = (0.5 + u) as f32 * used_width();
	let y = (0.5 - v) as f32 * screen_height();
	Vec2::new(x + left_offset(), y)
}

pub fn to_logical_size(size: Vec2) -> DVec2 {
	let (u, v) = (size.x / used_width(), size.y / screen_height());
	let x = u as f64 * SIZE_X;
	let y = v as f64 * SIZE_Y;
	DVec2::new(x, y)
}

pub fn to_logical_coords(pos: Vec2) -> DVec2 {
	let u = (pos.x - left_offset()) / used_width();
	let v = pos.y / screen_height();
	let x = (u - 0.5) as f64 * SIZE_X;
	let y = (0.5 - v) as f64 * SIZE_Y;
	clamp_pos(DVec2::new(x, y))
}

pub fn to_screen_rotation(rotation: f64) -> f32 {
	let pl = DVec2::new(rotation.cos(), rotation.sin());
	let ps = to_screen_size(pl);
	(-ps.y).atan2(ps.x)
}

pub fn scale_factor() -> Vec2 {
	let x = (SIZE_X as f32) / used_width();
	let y = (SIZE_Y as f32) / screen_height();
	Vec2::new(x, y)
}

pub fn clamp_pos(pos: DVec2) -> DVec2 {
	let hsz = DVec2::new(SIZE_X, SIZE_Y) * 0.5;
	pos.clamp(-hsz, hsz)
}

pub async fn load_utf8(loc: &str) -> Result<String, LoadError> {
	let f = load_file(&asset_loc(loc)).await.map_err(LoadError::file_error)?;
	let r = String::from_utf8(f).map_err(LoadError::read_error)?;
	Ok(r)
}

pub fn asset_loc(loc: &str) -> String {
	let mut out = String::from(ASSETS_DIR);
	out.push(path::MAIN_SEPARATOR);
	out.push_str(loc);
	out
}

pub fn chance(p: f64) -> bool {
	p >= rand::gen_range(0.0, 1.0)
}

pub trait RectOps: Copy {
	fn gen_in(self) -> DVec2;

	fn grow(self, amt: f64) -> Self;
}

impl RectOps for Rect {
	fn gen_in(self) -> DVec2 {
		let x = rand::gen_range(self.x, self.x + self.w);
		let y = rand::gen_range(self.y, self.y + self.h);
		DVec2::new(x as f64, y as f64)
	}

	fn grow(self, amt: f64) -> Self {
		let offset = amt as f32;
		let (x, y) = (self.x - offset, self.y - offset);
		let (w, h) = (self.w + 2.0 * offset, self.h + 2.0 * offset);
		Self::new(x, y, w, h)
	}
}

pub trait Drawable<T> {
	fn draw(&self, t: T);
}

pub trait Integrable {
	fn step(&mut self, dt: f64);
}

#[derive(Clone, Copy)]
pub struct Interval<T: Clone + Copy>(pub T, pub T);

impl Interval<f64> {
	pub fn in_range(self, t: f64) -> bool {
		t >= self.0 && t < self.1
	}

	pub fn intersection(self, other: Self) -> Option<Self> {
		let b = self.0 < other.1 && self.1 > other.0;
		b.then(|| Self(self.0.max(other.0), self.1.min(other.1)))
	}
}

#[derive(Debug)]
pub enum LoadError {
	File(FileError),
	Read(FromUtf8Error)
}

impl LoadError {
	pub fn file_error(f: FileError) -> Self {
		Self::File(f)
	}

	pub fn read_error(u: FromUtf8Error) -> Self {
		Self::Read(u)
	}
}
