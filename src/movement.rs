use crate::character::Character;
use crate::sprite::{AnimData, AnimFrame};
use crate::util::*;
use macroquad::prelude::*;

const MARGIN: f64 = 1.0;
const GATE_SIZE: (f64, f64) = (1.0, 12.0);
const GATE_LOC: &'static str = "gate.png";

pub fn left(size: DVec2, use_margin: bool) -> DVec2 {
	if use_margin {
		let margin = MARGIN + size.x * 0.5;
		DVec2::new(margin - SIZE_X * 0.5, 0.0)
	}
	else {
		DVec2::new((size.x - SIZE_X) * 0.5, 0.0)
	}
}

pub fn right(size: DVec2, use_margin: bool) -> DVec2 {
	if use_margin {
		let margin = MARGIN.max(size.x * 0.5);
		DVec2::new(SIZE_X - margin * 0.5, 0.0)
	}
	else {
		DVec2::new((SIZE_X - size.x) * 0.5, 0.0)
	}
}

pub fn allowed_region(size: DVec2) -> Rect {
	let s = DVec2::new(SIZE_X, SIZE_Y) - DVec2::splat(2.0 * MARGIN) - size * 2.0;
	Rect::new(-0.5 * s.x as f32, -0.5 * s.y as f32, s.x as f32, s.y as f32)
}

pub fn collides_left<T: Collidable>(t: &T) -> bool {
	let hb = t.hitbox();
	hb.lo.x < MARGIN - 0.5 * SIZE_X
}

pub fn collides_right<T: Collidable>(t: &T) -> bool {
	let hb = t.hitbox();
	hb.hi.x > 0.5 * SIZE_X - MARGIN
}

pub fn collides_top<T: Collidable>(t: &T) -> bool {
	let hb = t.hitbox();
	hb.hi.y > 0.5 * SIZE_Y - MARGIN
}

pub fn collides_bottom<T: Collidable>(t: &T) -> bool {
	let hb = t.hitbox();
	hb.lo.y < MARGIN - 0.5 * SIZE_Y
}

pub trait Collidable {
	fn hitbox(&self) -> HitBox;

	fn collides<T: Collidable>(&self, other: &T) -> bool {
		self.hitbox().collides(other.hitbox())
	}
}

impl Collidable for DVec2 {
	fn hitbox(&self) -> HitBox {
		HitBox::single_point(*self)
	}
}

#[derive(Clone, Copy)]
pub struct HitBox {
	pub lo: DVec2,
	pub hi: DVec2
}

impl HitBox {
	pub fn new(center: DVec2, size: DVec2) -> Self {
		Self {
			lo: center - size * 0.5,
			hi: center + size * 0.5
		}
	}

	pub fn single_point(pos: DVec2) -> Self {
		Self {
			lo: pos,
			hi: pos
		}
	}

	pub fn corners(&self) -> [DVec2; 4] {
		let br = DVec2::new(self.hi.x, self.lo.y);
		let tl = DVec2::new(self.lo.x, self.hi.y);
		[self.lo, br, self.hi, tl]
	}


	pub fn contains(&self, pos: DVec2) -> bool {
		let b0 = pos.x >= self.lo.x && pos.x < self.hi.x;
		let b1 = pos.y >= self.lo.y && pos.y < self.hi.y;
		b0 && b1
	}

	pub fn collides(&self, other: HitBox) -> bool {
		let b0 = other.lo.x >= self.hi.x || other.lo.y >= self.hi.y;
		let b1 = self.lo.x >= other.hi.x || self.lo.y >= other.hi.y;
		!(b0 || b1)
	}
}

impl Collidable for HitBox {
	fn hitbox(&self) -> HitBox {
		*self
	}
}

#[derive(Clone, Copy)]
#[repr(usize)]
pub enum GateAnim {
	Closed = 0,
	Open = 1
}

impl AnimFrame for GateAnim {
	fn frame(&self) -> usize {
		*self as usize
	}

	fn data() -> AnimData {
		AnimData::new(1, 2, 2)
	}
}

impl From<bool> for GateAnim {
	fn from(b: bool) -> Self {
		if b {
			Self::Open
		}
		else {
			Self::Closed
		}
	}
}

pub struct Gate {
	inner: Character,
	open: bool
}

impl Gate {
	pub async fn new(pos: DVec2) -> Self {
		let data = GateAnim::data();
		Self {
			inner: Character::new(GATE_LOC, data, pos, GATE_SIZE.into()).await,
			open: false
		}
	}

	pub async fn new_left() -> Self {
		Self::new(left(GATE_SIZE.into(), false)).await
	}

	pub async fn new_right() -> Self {
		Self::new(right(GATE_SIZE.into(), false)).await
	}

	pub fn set_open(&mut self, open: bool) {
		self.open = open;
		let anim: GateAnim = self.open.into();
		self.inner.set_frame(anim)
	}

	pub fn open(&self) -> bool {
		self.open
	}
}

impl Collidable for Gate {
	fn hitbox(&self) -> HitBox {
		self.inner.hitbox()
	}
}

impl Drawable<()> for Gate {
	fn draw(&self, _: ()) {
		self.inner.draw(())
	}
}
