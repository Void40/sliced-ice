use crate::movement::{Collidable, HitBox};
use crate::util::*;
use macroquad::prelude::*;
use polygon2::*;

const OUTER_THICKNESS: f32 = 6.0;
const INNER_THICKNESS: f32 = 3.0;
const LOWER_OFFSET: f32 = 1.0;
const OUTER_COLOR: Color = Color::new(0.353, 0.694, 0.831, 0.8);
const INNER_COLOR: Color = Color::new(0.153, 0.459, 0.581, 0.9);
const WATER_COLOR: Color = Color::new(0.125, 0.439, 0.557, 0.5);
const TOLERANCE: f64 = 1.0e-8;

pub fn cross(v0: DVec2, v1: DVec2) -> f64 {
	v0.x * v1.y - v0.y * v1.x
}

fn cmp(f: f64) -> Option<bool> {
	(f != 0.0).then(|| f > 0.0)
}

// Returns `Some(true)` if `v0` can be rotated anticlockwise to be parallel to
// `v` using an angle less than π, `Some(false)` if it is the other way round,
// and `None` if they are parallel.
pub fn classify(v0: DVec2, v1: DVec2) -> Option<bool> {
	cmp(cross(v0, v1))
}

// Returns `Some(true)` if `v0` and `v1` are separated by an angle of less than
// π, `Some(false)` if they are separated by more than π, or `None` if they are
// perpendicular
pub fn same_side(v0: DVec2, v1: DVec2) -> Option<bool> {
	cmp(v0.dot(v1))
}

fn vec_as_arr(v: DVec2) -> [f64; 2] {
	[v.x, v.y]
}

#[derive(Clone, Copy)]
pub struct Triangle(pub DVec2, pub DVec2, pub DVec2);

impl Drawable<Color> for Triangle {
	fn draw(&self, color: Color) {
		let vec2 = |v: DVec2| to_screen_coords(v);
		draw_triangle(vec2(self.0), vec2(self.1), vec2(self.2), color)
	}
}

pub struct Polygon {
	verts: Vec<[f64; 2]>,
	tris: Vec<Triangle>
}

impl Polygon {
	// `verts` need not be given in anti-clockwise order. Returns `None` if fewer
	// than 3 vertices were given.
	pub fn new(verts: Vec<DVec2>) -> Option<Self> {
		let n = verts.len();
		if n < 3 {
			return None
		};
		let mut vertcs = verts.clone();
		let (v0, v1) = (verts[1] - verts[0], verts[n - 1] - verts[n - 2]);
		match classify(v0, -v1) {
			Some(true) => (),
			// Polygon is specified backwards
			Some(false) => vertcs.reverse(),
			// First and last sides are parallel
			None => {
				vertcs.remove(n - 1);
				match same_side(v0, -v1) {
					// Remove degenerate section, then recurse
					Some(false) => (),
					_ => {
						vertcs.remove(0);
					}
				};
				return Self::new(vertcs)
			}
		};
		let mut tris = Vec::new();
		let vertas: Vec<_> = vertcs.into_iter().map(vec_as_arr).collect();
		let triangulation = triangulate(&vertas);
		for i in 0 .. triangulation.len() / 3 {
			let vert = |n| verts[triangulation[n]];
			tris.push(Triangle(vert(3 * i), vert(3 * i + 1), vert(3 * i + 2)))
		};
		let out = Self {
			verts: vertas,
			tris
		};
		Some(out)
	}

	pub fn contains<T: Collidable>(&self, obj: &T) -> bool {
		let cs = obj.hitbox().corners();
		cs.iter().all(|pos| contains_point(&self.verts, &vec_as_arr(*pos)))
	}

	pub fn area(&self) -> f64 {
		area(&self.verts)
	}
}

impl Drawable<Material> for Polygon {
	fn draw(&self, water_material: Material) {
		gl_use_material(water_material);
		for tri in &self.tris {
			tri.draw(WATER_COLOR)
		};
		gl_use_default_material()
	}
}

pub struct Trace {
	path: Vec<DVec2>,
	loops: Vec<Polygon>
}

impl Trace {
	pub fn new(start: DVec2) -> Self {
		Self {
			path: vec![start],
			loops: Vec::new()
		}
	}

	pub fn insert(&mut self, point: DVec2) {
		let n = self.path.len();
		let mut just_looped = false;
		if n > 0 {
			let last = *self.path.last().unwrap();
			if (last - point).length_squared() == 0.0 {
				return
			};
			let new = Segment(last, point);
			let mut prev = self.path[0];
			for i in 1 .. n {
				let old = Segment(prev, self.path[i]);
				prev = self.path[i];
				// Crossing point between `i - 1` and `i` found
				if let Some(pos) = old.crossing(new) {
					let mut newpath = Vec::from(&self.path[.. i]);
					let mut polyverts = Vec::from(&self.path[i ..]);
					newpath.push(pos);
					polyverts.push(pos);
					just_looped = true;
					self.path = newpath;
					if let Some(poly) = Polygon::new(polyverts) {
						self.loops.push(poly)
						// TODO play sound?
					};
					break
				}
			}
		};
		if n > 1 && !just_looped {
			let prev0 = self.path[n - 2];
			let prev1 = self.path[n - 1];
			if cross(prev1 - prev0, point - prev1).abs() < TOLERANCE {
				self.path[n - 1] = point;
				return
			}
		};
		self.path.push(point)
	}

	pub fn in_loop<T: Collidable>(&self, obj: &T) -> bool {
		(&self.loops).into_iter().any(|p: &Polygon| p.contains(obj))
	}
}

impl Drawable<Material> for Trace {
	fn draw(&self, water_material: Material) {
		(&self.loops).into_iter().for_each(|p: &Polygon| p.draw(water_material));
		let mut prev = self.path[0];
		for v in &self.path[1 ..] {
			Segment(prev, *v).draw(());
			prev = *v
		}
	}
}

#[derive(Clone, Copy)]
pub struct Segment(pub DVec2, pub DVec2);

impl Segment {
	// https://stackoverflow.com/questions/563198/
	pub fn crossing(self, other: Self) -> Option<DVec2> {
		/*TODO if !self.bounding_box().collides(other.bounding_box()) {
			return None
		};*/
		let (v0, v1): (DVec2, DVec2) = (self.disp(), other.disp());
		let (num, den) = (cross(other.0 - self.0, v0), cross(v0, v1));
		let num2 = cross(other.0 - self.0, v1);
		if den == 0.0 {
			if num2 == 0.0 {
				let v0no = v0.try_normalize();
				if let Some(v0n) = v0no {
					let proj = |v: DVec2| v.dot(v0n);
					let ip = Interval(proj(self.0), proj(self.1));
					let io = Interval(proj(other.0), proj(other.1));
					let mp = ip.intersection(io).map(|i| (i.0 + i.1) * 0.5 - ip.0);
					mp.map(|t| self.0 + v0 * t)
				}
				else {
					None
				}
			}
			else {
				None
			}
		}
		else {
			let t = num / den;
			let r = Interval(0.0, 1.0);
			(r.in_range(t) && r.in_range(num2 / den)).then(|| self.0 + v0 * t)
		}
	}

	pub fn disp(self) -> DVec2 {
		self.1 - self.0
	}

	pub fn length(self) -> f64 {
		self.disp().length()
	}

	pub fn lerp_in(self, t: f64) -> DVec2 {
		self.0.lerp(self.1, t)
	}

	pub fn bounding_box(self) -> HitBox {
		HitBox {
			lo: self.0,
			hi: self.1
		}
	}
}

impl Drawable<()> for Segment {
	fn draw(&self, _: ()) {
		let (p0, p1) = (to_screen_coords(self.0), to_screen_coords(self.1));
		let c0 = (p0.x as f32, p0.y as f32);
		let c1 = (p1.x as f32, p1.y as f32);
		draw_line(c0.0, c0.1, c1.0, c1.1, OUTER_THICKNESS, OUTER_COLOR);
		let c0i = (c0.0 + LOWER_OFFSET, c0.1 + LOWER_OFFSET);
		let c1i = (c1.0 + LOWER_OFFSET, c1.1 + LOWER_OFFSET);
		draw_line(c0i.0, c0i.1, c1i.0, c1i.1, INNER_THICKNESS, INNER_COLOR)
	}
}
