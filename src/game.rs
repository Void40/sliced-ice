use crate::character::{Ambient, AmbientType, Enemy, EnemyType, Player};
use crate::geometry::Trace;
use crate::movement::{Collidable, Gate};
use crate::sprite::{AnimFrame, InstanceData, NoAnim, Sprite};
use crate::util::*;
use std::fmt;
use macroquad::prelude::*;

const STAT_COST: u32 = 1;
const LIN_COST: u32 = 2;
const ROT_COST: u32 = 3;
const BG_LOC: &'static str = "background.png";
const LC_LOC: &'static str = "counter.png";
const AMBIENT_COUNT: usize = 5;
const ENEMY_BUDGET: f64 = 1.0;
const ENEMY_SCALING: f64 = 0.5;
const WAIT_TIME: f64 = 0.5;
const LC_SIZE: (f64, f64) = (4.0, 2.0);
const LC_MARGINS: (f64, f64) = (0.25, 1.0);
const LC_FONT_SIZE: f32 = 30.0;
const GAME_OVER_BOX: (f64, f64) = (10.0, 10.0);
const GAME_OVER_MARGINS: (f64, f64) = (1.0, 2.0);
const GAME_OVER_FONT_SIZE: f32 = 60.0;
const RESTART_FONT_SIZE: f32 = 30.0;
const GAME_OVER_COLOR: Color = Color::new(0.5, 0.5, 0.5, 0.9);

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum GameState {
	Running,
	LevelTransition,
	Dead
}

pub struct LevelCounter {
	inner: Sprite
}

impl LevelCounter {
	pub async fn new() -> Self {
		Self {
			inner: Sprite::new(&LC_LOC, NoAnim::data()).await
		}
	}
}

impl Drawable<u32> for LevelCounter {
	fn draw(&self, level: u32) {
		let size = DVec2::from(LC_SIZE);
		let bl = DVec2::new(SIZE_X, SIZE_Y) * 0.5 - size;
		let data = InstanceData::new_aligned(bl + size * 0.5, size);
		self.inner.draw(data);
		let tp = DVec2::new(bl.x + LC_MARGINS.0, SIZE_Y * 0.5 - LC_MARGINS.1);
		let text_pos = to_screen_coords(tp);
		let text = fmt::format(format_args!("Level: {}", level));
		draw_text(&text, text_pos.x, text_pos.y, LC_FONT_SIZE, BLACK)
	}
}

pub struct World {
	player: Player,
	enemies: Vec<Enemy>,
	ambients: Vec<Ambient>,
	gates: [Gate; 2],
	trace: Trace,
	bg: Sprite,
	level_counter: LevelCounter,
	level: u32,
	state: GameState,
	wait_time: f64,
	pub splash: Option<DVec2>
}

impl World {
	pub async fn new() -> Self {
		let player = Player::new().await;
		let mut out = Self {
			enemies: Vec::new(),
			ambients: Vec::new(),
			gates: [Gate::new_left().await, Gate::new_right().await],
			trace: Trace::new(player.pos()),
			player,
			bg: Sprite::new(&BG_LOC, NoAnim::data()).await,
			level_counter: LevelCounter::new().await,
			level: 0,
			state: GameState::Running,
			wait_time: 0.0,
			splash: None
		};
		out.gen_ambients().await;
		out
	}

	pub async fn gen_ambients(&mut self) {
		self.ambients = Vec::new();
		for _ in 0 .. AMBIENT_COUNT {
			self.ambients.push(Ambient::new(AmbientType::gen_fish()).await)
		}
	}

	pub async fn add_enemy(&mut self, budget: u32) -> u32 {
		let ws = STAT_COST as f64 / budget as f64;
		let wl = LIN_COST as f64 / budget as f64;
		let wr = ROT_COST as f64 / budget as f64;
		let (ps, pl) = (ws / (ws + wl + wr), wl / (wl + wr));
		let cost = if chance(ps) {
			self.enemies.push(Enemy::new(EnemyType::gen_stationary()).await);
			STAT_COST
		}
		else if chance(pl) {
			self.enemies.push(Enemy::new(EnemyType::gen_linear()).await);
			LIN_COST
		}
		else {
			self.enemies.push(Enemy::new(EnemyType::gen_rotating()).await);
			ROT_COST
		};
		budget.saturating_sub(cost)
	}

	pub async fn next_level(&mut self) {
		self.player.respawn();
		self.enemies = Vec::new();
		self.gen_ambients().await;
		self.gates[1].set_open(false);
		self.trace = Trace::new(self.player.pos());
		self.level += 1;
		self.state = GameState::LevelTransition;
		self.splash = None;
		match self.level {
			0 => return,
			1 => self.enemies.push(Enemy::new(EnemyType::gen_stationary()).await),
			2 => self.enemies.push(Enemy::new(EnemyType::gen_linear()).await),
			3 => self.enemies.push(Enemy::new(EnemyType::gen_rotating()).await),
			_ => {
				let b = ENEMY_BUDGET * (self.level as f64).powf(ENEMY_SCALING);
				let mut budget = b.ceil() as u32;
				while budget > 0 {
					budget = self.add_enemy(budget).await
				}
			}
		}
	}

	pub async fn step(&mut self, dt: f64) {
		match self.state {
			GameState::Running => {
				self.player.step(dt);
				self.trace.insert(self.player.pos());
				if self.trace.in_loop(&self.player) {
					self.state = GameState::Dead;
					self.splash = Some(self.player.pos());
					// TODO play sound
					return
				};
				let mut i = 0;
				while i < self.enemies.len() {
					self.enemies[i].step(dt);
					if self.player.collides(&self.enemies[i]) {
						self.state = GameState::Dead;
						// TODO play sound
						return
					};
					if self.trace.in_loop(&self.enemies[i]) {
						self.splash = Some(self.enemies[i].pos());
						self.enemies.remove(i);
					}
					else {
						i += 1
					}
				};
				if self.enemies.len() == 0 {
					self.gates[1].set_open(true)
				};
				if self.player.collides(&self.gates[0]) && self.player.vel.x < 0.0 {
					self.player.request_side_bounce()
					// TODO play sound?
				};
				if self.player.collides(&self.gates[1]) && self.player.vel.x > 0.0 {
					if self.gates[1].open() {
						return self.next_level().await
						// TODO play sound?
					}
					else {
						self.player.request_side_bounce()
						// TODO play sound?
					}
				}
				(&mut self.ambients).iter_mut().for_each(|a: &mut Ambient| a.step(dt));
			},
			GameState::Dead => {
				self.wait_time += dt;
				if self.wait_time >= WAIT_TIME && is_key_down(KeyCode::R) {
					*self = Self::new().await
				}
			},
			GameState::LevelTransition => {
				self.wait_time += dt;
				if self.wait_time >= WAIT_TIME {
					self.wait_time = 0.0;
					self.state = GameState::Running
				}
			}
		}
	}

	pub fn take_splash(&mut self) -> Option<DVec2> {
		let splash = self.splash;
		self.splash = None;
		splash
	}
}

impl Drawable<Material> for World {
	fn draw(&self, water_material: Material) {
		match self.state {
			GameState::LevelTransition => (),
			_ => {
				let (pos, size) = whole_area();
				let data = InstanceData::new_aligned(pos, size);
				self.bg.draw(data);
				(&self.ambients).into_iter().for_each(|a: &Ambient| a.draw(()));
				(&self.enemies).into_iter().for_each(|e: &Enemy| e.draw(()));
				self.trace.draw(water_material);
				self.player.draw(());
				self.gates[0].draw(());
				self.gates[1].draw(());
				self.level_counter.draw(self.level);
				if let GameState::Dead = self.state {
					// TODO game over screen
					let size = DVec2::from(GAME_OVER_BOX);
					let tl = size * DVec2::new(-0.5, 0.5);
					let tp = tl + DVec2::new(GAME_OVER_MARGINS.0, -GAME_OVER_MARGINS.1);
					let text_pos0 = to_screen_coords(tp);
					let pos = to_screen_coords(tl);
					let ssize = to_screen_size(size);
					draw_rectangle(pos.x, pos.y, ssize.x, ssize.y, GAME_OVER_COLOR);
					let text0 = "Game Over!";
					draw_text(text0, text_pos0.x, text_pos0.y, GAME_OVER_FONT_SIZE, RED);
					let text1 = "Press 'R' to restart";
					let text_pos1 = text_pos0 + Vec2::Y * GAME_OVER_FONT_SIZE;
					draw_text(text1, text_pos1.x, text_pos1.y, RESTART_FONT_SIZE, BLACK);
				}
			}
		}
	}
}
