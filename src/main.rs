extern crate macroquad;
extern crate polygon2;

pub mod character;
pub mod game;
pub mod geometry;
pub mod material;
pub mod movement;
pub mod shader;
pub mod sprite;
pub mod util;

use game::World;
use material::WaterData;
use util::*;
use macroquad::prelude::*;

fn window_conf() -> Conf {
    Conf {
        window_title: "Sliced Ice".to_owned(),
        high_dpi: true,
        .. Default::default()
    }
}

#[macroquad::main(window_conf)]
async fn main() {
	let water_material = WaterData::create_material().await;
	let mut water_data = WaterData::new();
	let mut world = World::new().await;
	loop {
		clear_background(BLACK);
		let dt = get_frame_time() as f64;
		world.step(dt).await;
		if let Some(pos) = world.take_splash() {
			water_data.create_splash(pos)
		};
		water_data.update_material(water_material);
		world.draw(water_material);
		next_frame().await
	}
}
