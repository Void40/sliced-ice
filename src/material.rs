use crate::shader::Shaders;
use crate::util::*;
use macroquad::prelude::*;
use macroquad::rand;

const SPLASHES: &'static str = "splashes";
const SPLASH_TIMES: &'static str = "splashTimes";
const ACTIVE: &'static str = "active";
const COORD_SF: &'static str = "coordScaleFactor";
const WIND: &'static str = "wind";
const TIME: &'static str = "time";
const WATER_FS_LOC: &'static str = "water.glslf";
const MAX_WIND_SPEED: f32 = 0.5;
const WIND_SKIPS: u32 = 600;
const WIND_LERP_FACTOR: f32 = 0.1;

pub struct WaterData {
	splashes: [Option<Vec2>; 4],
	splash_times: [f32; 4],
	count: usize,
	wind: Vec2,
	time: f32,
	wind_skips: u32,
	next_wind: Vec2,
	prev_wind: Vec2,
	wind_time: f32
}

impl WaterData {
	pub fn new() -> Self {
		let wind = Self::new_wind();
		Self {
			splashes: [None; 4],
			splash_times: [0.0; 4],
			count: 0,
			wind: wind,
			time: 0.0,
			wind_skips: 0,
			next_wind: Self::new_wind(),
			prev_wind: wind,
			wind_time: 0.0
		}
	}

	fn new_wind() -> Vec2 {
		let r = || rand::gen_range(-1.0f32, 1.0f32);
		Vec2::new(r(), r()) * MAX_WIND_SPEED
	}

	pub fn create_splash(&mut self, pos: DVec2) {
		let lowp = Some(Vec2::new(pos.x as f32, pos.y as f32));
		if self.count < 4 {
			self.splashes[self.count] = lowp;
			self.splash_times[self.count] = 0.0;
			self.count += 1
		}
		else {
			let mut max: f32 = 0.0;
			for t in self.splash_times {
				max = max.max(t)
			};
			for i in 0 .. 4 {
				if self.splash_times[i] == max {
					self.splashes[i] = lowp;
					self.splash_times[i] = 0.0
				}
			}
		}
	}

	pub async fn create_material() -> Material {
		let shaders = Shaders::new(None, Some(WATER_FS_LOC)).await;
		let splashes = (String::from(SPLASHES), UniformType::Mat4);
		let splash_times = (String::from(SPLASH_TIMES), UniformType::Float4);
		let active = (String::from(ACTIVE), UniformType::Int4);
		let coord_sf = (String::from(COORD_SF), UniformType::Float2);
		let wind = (String::from(WIND), UniformType::Float2);
		let time = (String::from(TIME), UniformType::Float1);
		let us = vec![splashes, splash_times, active, coord_sf, wind, time];
		shaders.load_material(us)
	}

	pub fn update_material(&self, material: Material) {
		let mut splashes = [[0.0; 4]; 4];
		let mut active: [u32; 4] = [0; 4];
		for i in 0 .. 4 {
			if let Some(pos) = self.splashes[i] {
				splashes[i][0] = pos.x;
				splashes[i][1] = pos.y;
				active[i] = 1
			}
		};
		material.set_uniform(SPLASHES, splashes);
		material.set_uniform(SPLASH_TIMES, self.splash_times);
		material.set_uniform(ACTIVE, active);
		material.set_uniform(COORD_SF, scale_factor());
		material.set_uniform(WIND, self.wind);
		material.set_uniform(TIME, self.time)
	}
}

impl Integrable for WaterData {
	fn step(&mut self, dt: f64) {
		let dtf32 = dt as f32;
		self.splash_times.iter_mut().for_each(|t: &mut f32| *t += dtf32);
		self.wind_skips += 1;
		let t = WIND_LERP_FACTOR * self.wind_time;
		self.wind = self.prev_wind.lerp(self.next_wind, t);
		if self.wind_skips == WIND_SKIPS {
			self.next_wind = Self::new_wind();
			self.prev_wind = self.wind;
			self.wind_skips = 0;
			self.wind_time = 0.0;
		};
		self.time += dtf32;
		self.wind_time += dtf32;
	}
}
