use crate::util::*;
use macroquad::prelude::*;

pub trait AnimFrame {
	fn frame(&self) -> usize;

	fn data() -> AnimData;
}

#[derive(Clone, Copy)]
pub struct AnimData {
	pub grid_shape: (usize, usize),
	pub frames_len: usize
}

impl AnimData {
	pub fn single() -> Self {
		Self {
			grid_shape: (1, 1),
			frames_len: 1
		}
	}

	pub fn new(w: usize, h: usize, n: usize) -> Self {
		if n > w * h {
			panic!("Too many frames for given grid size")
		};
		Self {
			grid_shape: (w, h),
			frames_len: n
		}
	}
}

#[derive(Clone, Copy)]
pub struct NoAnim;

impl AnimFrame for NoAnim {
	fn frame(&self) -> usize {
		0
	}

	fn data() -> AnimData {
		AnimData::single()
	}
}

#[derive(Clone)]
pub struct Sprite {
	anim_data: AnimData,
	texture: Texture2D,
	frame: usize
}

impl Sprite {
	pub async fn new(loc: &str, anim_data: AnimData) -> Self {
		let texture = load_texture(&asset_loc(loc)).await.unwrap();
		Self {
			anim_data,
			texture,
			frame: 0
		}
	}

	pub fn set_frame<T: AnimFrame>(&mut self, anim_frame: T) {
		let frame = anim_frame.frame();
		if frame < self.anim_data.frames_len {
			self.frame = frame;
		}
		else {
			panic!("Invalid frame index")
		}
	}
}

#[derive(Clone, Copy)]
pub struct InstanceData {
	pub pos: DVec2,
	pub size: DVec2,
	pub rotation: f64
}

impl InstanceData {
	pub fn new_aligned(pos: DVec2, size: DVec2) -> Self {
		Self {
			pos,
			size,
			rotation: 0.0
		}
	}
}


impl Drawable<InstanceData> for Sprite {
	fn draw(&self, data: InstanceData) {
		let params = DrawTextureParams {
			dest_size: Some(to_screen_size(data.size)),
			source: {
				let (wf, hf) = (self.texture.width(), self.texture.height());
				let (wi, hi) = self.anim_data.grid_shape;
				let (sx, sy) = (wf / wi as f32, hf / hi as f32);
				let (xi, yi) = (self.frame % wi, self.frame / wi);
				let (xlf, ylf) = (xi as f32 * sx, yi as f32 * sy);
				Some(Rect::new(xlf, ylf, sx, sy))
			},
			rotation: to_screen_rotation(data.rotation),
			.. Default::default()
		};
		let tl = data.pos + DVec2::new(-data.size.x, data.size.y) * 0.5;
		let screen_pos = to_screen_coords(tl);
		draw_texture_ex(self.texture, screen_pos.x, screen_pos.y, WHITE, params)
	}
}
