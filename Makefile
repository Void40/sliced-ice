TARGET = wasm32-unknown-unknown
INFILE = sliced-ice

debug: src
	clear
	cargo build --target $(TARGET)
	cp target/$(TARGET)/debug/$(INFILE).wasm wasm/bin.wasm

release: src
	clear
	cargo build --target $(TARGET) --release
	cp target/$(TARGET)/release/$(INFILE).wasm wasm/bin.wasm

host: wasm
	clear
	make cpassets
	firefox localhost:4000
	~/.cargo/bin/basic-http-server wasm

test: src
	clear
	cargo test

cpassets: assets
	cp -r assets wasm

clean: target wasm/bin.wasm
	clear
	cargo clean
	rm wasm/bin.wasm
