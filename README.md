## \[Sliced Ice\]

A game by Void4
Entry for Ludum Dare 49 (Theme: Unstable)

Made with:  
- [macroquad](https://crates.io/crates/macroquad) (Portable Rust game engine)
- [polygon2](https://crates.io/crates/polygon2) (Library for polygon algorithms
like triangulation)

# Compiling
See *./Makefile*. You may wish to change the `TARGET` variable if you want to 
compile to a native binary for example. If you want to compile to WebAssembly 
then `make release` and follow the macroquad 
[build instructions](https://github.com/not-fl3/macroquad#build-instructions).

# License Info
This game is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

The GPLv3 license can be found in the file *./LICENSE_GPL3*. The files
*./wasm/index.html* and *./wasm/mq\_js\_bundle.js* originate from 
[macroquad](https://crates.io/crates/macroquad), which is dual-licensed under 
MIT/Apache-2.0 (see *./wasm/LICENSE-MIT* and *./wasm/LICENSE-APACHE*).
