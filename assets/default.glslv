#version 100
precision lowp float;

attribute vec3 position;
attribute vec2 texcoord;
attribute vec4 color0;

varying vec2 uv;
varying vec2 pos;
varying vec4 color;

uniform mat4 Model;
uniform mat4 Projection;

void main() {
	gl_Position = Projection * Model * vec4(position, 1);
	pos = position.xy;
	uv = texcoord;
	color = color0 / 255.0;
}
