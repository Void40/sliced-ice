#version 100
precision lowp float;

varying vec4 color;
varying vec2 uv;
varying vec2 pos;

uniform sampler2D Texture;

void main() {
	gl_FragColor = color * texture2D(Texture, uv);
}
