#version 100
precision lowp float;

varying vec4 color;
varying vec2 uv;
varying vec2 pos;

uniform sampler2D Texture;
uniform mat4 splashes;
uniform vec4 splashTimes;
uniform ivec4 active;
uniform vec2 coordScaleFactor;
uniform vec2 wind;
uniform float time;

const float WAVE_SPEED = 0.99;
const float MAX_HEIGHT = 0.1;
const float WAVENUMBER = 6.28;
const float DECAY_RATE = 0.3;
const float WIND_WAVENUMBER = 31.4;
const float WIND_AMPLITUDE = 0.01;

float heightWind() {
	float windSpeed = length(wind);
	float xr = dot(wind / windSpeed, pos * coordScaleFactor) - windSpeed * time;
	return WIND_AMPLITUDE * sin(WIND_WAVENUMBER * xr);
}

float height(float x, float t) {
	float env = min(pow(x, -0.5), MAX_HEIGHT) * exp(-DECAY_RATE * t);
	float xr = (x - WAVE_SPEED * t);
	if(xr > 0.0) env = 0.0;
	return env * sin(WAVENUMBER * xr); 
}

float dist(vec2 v) {
	return length(pos * coordScaleFactor - v);
}

void main() {
	float h = 0.0;
	if(active.x != 0) h += height(dist(splashes[0].xy), splashTimes.x);
	if(active.y != 0) h += height(dist(splashes[1].xy), splashTimes.y);
	if(active.z != 0) h += height(dist(splashes[2].xy), splashTimes.z);
	if(active.w != 0) h += height(dist(splashes[3].xy), splashTimes.w);
	h = min(h, MAX_HEIGHT);
	h += heightWind();
	gl_FragColor = color * texture2D(Texture, uv) * vec4(vec3(1.0 - h), 1.0);
}
